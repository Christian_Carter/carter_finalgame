﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FollowScript : MonoBehaviour
{
    public Transform FollowObject;

    // Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = new Vector3(FollowObject.position.x, transform.position.y, transform.position.z);
        transform.position = pos;
    }
}
