﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movingtiles : MonoBehaviour
{

    [Header("Tile Speed and Location")]
    [SerializeField] float moveSpeed;
    [SerializeField] float minX;
    [SerializeField] float maxX;
    bool moveRight = true;


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.collider.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.collider.transform.SetParent(null);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > maxX)
        {
            moveRight = false;
        }
        if (transform.position.x < minX)
        {
            moveRight = true;
        }
        if (moveRight)
        {
            transform.position = new Vector2(transform.position.x + moveSpeed * Time.deltaTime, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(transform.position.x - moveSpeed * Time.deltaTime, transform.position.y);
        }
    }
}
