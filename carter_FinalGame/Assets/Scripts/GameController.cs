﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    int health = 6;
    int bosshealth = 6;
    public Sprite halfheart;
    public Sprite emptyheart;
    //hearts right to left
    public Image heartgameobject1;
    public Image heartgameobject2;
    public Image heartgameobject3;

    public Image bossheartgameobject1;
    public Image bossheartgameobject2;
    public Image bossheartgameobject3;

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health == 5)
        {
            heartgameobject3.sprite = halfheart;
        }
        else if (health == 4)
        {
            heartgameobject3.sprite = emptyheart;
        }
        else if (health == 3)
        {
            heartgameobject2.sprite = halfheart;
        }
        else if (health == 2)
        {
            heartgameobject2.sprite = emptyheart;
        }
        else if (health == 1)
        {
            heartgameobject1.sprite = halfheart;
        }
        else
        {
            SceneManager.LoadScene(6);
        }
    }

    public void BossTakeDamage(int damage)
    {
        bosshealth -= damage;

        if (bosshealth == 5)
        {
            bossheartgameobject3.sprite = halfheart;
        }
        else if (bosshealth == 4)
        {
            bossheartgameobject3.sprite = emptyheart;
        }
        else if (bosshealth == 3)
        {
            bossheartgameobject2.sprite = halfheart;
        }
        else if (bosshealth == 2)
        {
            bossheartgameobject2.sprite = emptyheart;
        }
        else if (bosshealth == 1)
        {
           bossheartgameobject1.sprite = halfheart;
        }
        else
        {
            SceneManager.LoadScene(10);
        }
    }
}
