﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2EnemyShootingScript : MonoBehaviour
{
    [SerializeField] bool shooting = true;
    [SerializeField] float ShootTime = 5f;
    [SerializeField] float currentTime = 0f;
    [SerializeField] GameObject projectilePrefab;
    // Start is called before the first frame update
    void Start()
    {
        currentTime = ShootTime;
        Debug.Log(ShootTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (!shooting)
        {
            Debug.Log("Shoot");
            currentTime -= Time.deltaTime;
            if (currentTime <= 0)
            {
                currentTime = ShootTime;
                Shoot();
            }
        }

    }

    public void Shoot()
    {
        shooting = false;
        Instantiate(projectilePrefab, transform.position, projectilePrefab.transform.rotation);
    }

}
