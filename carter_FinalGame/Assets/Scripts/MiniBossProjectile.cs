﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBossProjectile : MonoBehaviour
{
    float projectile_life_time = 15f;
    public float shotspeed = 10f;
    Player player;
    GameController gamecontroller;

    public Rigidbody2D rB2D;

    private void Start()
    {
       // player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        rB2D = GetComponent<Rigidbody2D>();
        gamecontroller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    private void Update()
    {
        projectile_life_time -= Time.deltaTime;
        if (projectile_life_time <= 0)
        {

            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        rB2D.velocity = -transform.right * shotspeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            gamecontroller.TakeDamage(1);
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("MovingTiles"))
        {

        }
    }
}
