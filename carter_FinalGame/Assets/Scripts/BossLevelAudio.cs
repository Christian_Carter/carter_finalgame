﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossLevelAudio : MonoBehaviour
{
    public static Audio check;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        if (sceneName == "Lose Screen")
        {
            Destroy(gameObject);
        }
    }
}
