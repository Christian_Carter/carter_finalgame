﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    float projectile_life_time = 3.5f;
    public float shotspeed = 10f;
    Player player;
    bool movingright = false;

    public Rigidbody2D rB2D;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        if (player.turn)
        {
            movingright = false;
        }
        else
        {
            movingright = true;
        }

        rB2D = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        projectile_life_time -= Time.deltaTime;
        if (projectile_life_time <= 0)
        {
            
            Destroy(gameObject);
        }
    }
    private void FixedUpdate()
    {
        if (movingright)
        {
            rB2D.velocity = transform.right * shotspeed;
        }
        else
        {
            rB2D.velocity = -transform.right * shotspeed;
        }
    }
    private void OnDestroy()
    {
        player.AddBullet();
    }
}