﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level2Part1EnemyMovementScript : MonoBehaviour
{

    [Header("Enemy Speed and Location")]
    [SerializeField] float moveSpeed;
    [SerializeField] float minY;
    [SerializeField] float maxY;
    public bool moveUp = true;


    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > maxY)
        {
            moveUp= false;
        }
        if (transform.position.y < minY)
        {
            moveUp= true;
        }
        if (moveUp)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + moveSpeed * Time.deltaTime);
        }
        else
        {
            transform.position = new Vector2(transform.position.x, transform.position.y - moveSpeed * Time.deltaTime);
        }
    }
}