﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideHouse : MonoBehaviour
{
    public GameObject UpstairsCollider;
    // Start is called before the first frame update
    void Start()
    {
        UpstairsCollider.SetActive(false); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            UpstairsCollider.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            UpstairsCollider.SetActive(false);
        }
    }


}
