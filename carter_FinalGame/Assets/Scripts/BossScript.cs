﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Time.timescale = 0;
//Time.timescale = 1;
public class BossScript : MonoBehaviour
{
    GameController gamecontroller;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("PlayerProjectile"))
        {
            gamecontroller.BossTakeDamage(1);
        }
    }

}
