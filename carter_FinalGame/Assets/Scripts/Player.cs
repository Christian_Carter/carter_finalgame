﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    Rigidbody2D rB2D;

    [Header("Projectile")]
    public GameObject projectilePrefab;
    public Transform shootspawn;
    public float shotspeed = 10f;
    public int bullet_count = 3;
    public TextMeshProUGUI counttext;
    public bool turn = false;

    [Header("Player Speed and Jump")]
    public float runSpeed;
    public float jumpSpeed;
    public bool doublejump = false;
    

    public Animator animator;

    public SpriteRenderer spriteRenderer;

    //Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {

        int levelMask = LayerMask.GetMask("Level");
        bool onground = Physics2D.BoxCast(transform.position, new Vector2(1f, 1f), 0f, Vector2.down, .01f, levelMask);
        animator.SetBool("onground", onground);
        if (onground)
        {
            doublejump = false;
        }
        //If the player the jump button:
        if (Input.GetButtonDown("Jump"))
        {
            //Do a box cat of a very thin box our width directly below us to check if we are on a floor
            //Get the layer mask the player can jump from [make sure colliders are to "Level" laye

            //Parameters: (position of center of box, size of box, direction to cast, layer mask to check)
            if (onground)
            {
                Jump();
            }
            else if (!onground && !doublejump)
            {
                doublejump = true;
                Jump();
            }
        }
        if (Input.GetButtonDown("Shoot"))
        {
            Debug.Log(bullet_count);
            if (bullet_count > 0)
            {
                animator.SetBool("IsShooting", true);

                Debug.Log("Pressed_Space");
                Instantiate(projectilePrefab, shootspawn.transform.position, projectilePrefab.transform.rotation);
                Physics2D.IgnoreLayerCollision(9, 10);

                bullet_count -= 1;
                counttext.text = "Projectiles: " + bullet_count;
                Debug.Log(bullet_count);
            }


        }
        else
        {
            animator.SetBool("IsShooting", false);
        }
    }

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal") * runSpeed;

        rB2D.velocity = new Vector2(horizontalInput * Time.deltaTime, rB2D.velocity.y);

        animator.SetFloat("Speed", Mathf.Abs(horizontalInput));

        if (rB2D.velocity.x > 0)
        {
            spriteRenderer.flipX = false;
            turn = false;
        }
        else
        if (rB2D.velocity.x < 0)
        {
            spriteRenderer.flipX = true;
            turn = true;
        }

    }

    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpSpeed);
    }

    public void AddBullet()
    {
        bullet_count++;
        counttext.text = "Projectiles: " + bullet_count;
    }
}