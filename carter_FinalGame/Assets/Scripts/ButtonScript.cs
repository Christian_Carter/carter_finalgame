﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour
{
    public int count = 0;
    public void changescene()
    {
        SceneManager.LoadScene(1);
        Debug.Log(count);
    }

    public void changetostory2()
    {
        SceneManager.LoadScene(2);
        Debug.Log(count);
    }

    public void changetostory3()
    {
        SceneManager.LoadScene(3);
        Debug.Log(count);
    }

    public void changetoinstructions()
    {
        SceneManager.LoadScene(4);
        Debug.Log(count);
    }

    public void changetolevel1()
    {
        SceneManager.LoadScene(5);
        count += 1;
        Debug.Log(count);
    }

    public void quitgame()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
        Debug.Log("quit");
    }
    
}
