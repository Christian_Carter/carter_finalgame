﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WrongWayScript : MonoBehaviour
{
    public GameObject WrongWayCanvas;

    private void Start()
    {
        WrongWayCanvas.SetActive(false);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            WrongWayCanvas.SetActive(true);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            WrongWayCanvas.SetActive(false);
        }
    }
}
