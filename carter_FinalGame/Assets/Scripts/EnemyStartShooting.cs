﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStartShooting : MonoBehaviour
{
    public Level2EnemyShootingScript enemy1;
    public Level2EnemyShootingScript enemy2;
    public Level2EnemyShootingScript enemy3;

    bool hastriggered = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (!hastriggered)
            {
                enemy1.Shoot();
                enemy2.Shoot();
                enemy3.Shoot();
                hastriggered = true;
            }
        }
    }
}
