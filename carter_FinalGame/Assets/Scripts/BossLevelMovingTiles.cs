﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLevelMovingTiles : MonoBehaviour
{

    [Header("Enemy Speed and Location")]
    [SerializeField] float moveSpeed;
    [SerializeField] float minY;
    [SerializeField] float maxY;
    public bool moveUp = true;


    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > maxY)
        {
            moveUp = false;
        }
        if (transform.position.y < minY)
        {
            moveUp = true;
        }
        if (moveUp)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + moveSpeed * Time.deltaTime);
        }
        else
        {
            transform.position = new Vector2(transform.position.x, transform.position.y - moveSpeed * Time.deltaTime);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.collider.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.collider.transform.SetParent(null);
        }
    }
}
