﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level2Transition : MonoBehaviour
{
    public float currentTime = 0f;
    public float startingTime = 6f;
    public bool Stop;

    //setting currentTime = startingTime
    void Start()
    {
        currentTime = startingTime;
        Debug.Log(currentTime);
    }

    void Update()

    {
        //if the timer hasn't stopped, the timer will keep going
        if (Stop == false)
        {
            //makes the timer countdown every second
            currentTime -= Time.deltaTime;
            //this keeps the timer from counting down past 0 seconds.
            if (currentTime <= 0)
            {
                currentTime = 0;
                SceneManager.LoadScene(8);
            }
        }
    }
}
